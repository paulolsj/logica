import java.util.Scanner;

public class Problema3 {
    //
    public static void main(String[] args) {

        Scanner leitura = new Scanner(System.in);

        System.out.println("Digite um numero em ROMANO: ");
        int entrada = leitura.nextInt();

        String saida = numRomano(entrada);
        System.out.printf("Valor Lido = %d Valor de Saida = %s ", entrada, saida);
    }

    public static String numRomano(int num) {

        String romano = "";
        char ch;
        int pot = 1000;
        int dig;

        while(num > 0){
            dig = num/pot;
            num %= pot;
            if (dig > 0){
                if((dig>=5)&&(dig<9)) {
                    dig -= 5;
                    ch = letraValor(pot*5);
                    romano += ch;
                }
                if((dig < 4)){
                    ch = letraValor(pot);
                    for (int i = 0; i < dig; i++) {
                        romano += ch;
                    }
                }
                if ((dig == 4)||(dig == 9)) {
                    ch = letraValor(pot);
                    romano += ch;
                    ch = (dig == 9) ? letraValor(pot*10) : letraValor(pot*5);
                    romano += ch;
                }
            }
            pot /= 10;
        }

        return romano;
    }

    public static char letraValor(int num) {

        switch (num){
            case 1000:
                return 'M';

            case 500:
                return 'D';

            case 100:
                return 'C';

            case 50:
                return 'L';

            case 10:
                return 'X';

            case 5:
                return 'V';

            case 1:
                return 'I';
        }
        return ' ';
    }
}
