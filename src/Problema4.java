import java.util.Scanner;

public class Problema4 {
    //
    public static void main(String[] args) {

        Scanner leitura = new Scanner(System.in);

        System.out.println("Digite um numero em ROMANO: ");
        String entrada = leitura.next();

        int saida = numRomano(entrada);
        System.out.printf("Valor Lido = %s Valor de Saida = %d ", entrada, saida);
    }

    public static int numRomano(String romano) {

        int tam = romano.length();

        int num = (tam > 0) ? valorLetra(romano,tam-1) : 0;
        int valaux = num;

        if(tam > 1){
            for (int i = tam-2; i >= 0 ; i--) {
                int valch = valorLetra(romano,i);
                num = (valaux > valch) ? num - valch : num + valch;
                valaux = valch;
            }
        }

        return num;
    }

    public static int valorLetra(String romano, int pos) {

        char ch = romano.charAt(pos);
        switch (ch){
            case 'M':
                return 1000;

            case 'D':
                return 500;

            case 'C':
                return 100;

            case 'L':
                return 50;

            case 'X':
                return 10;

            case 'V':
                return 5;

            case 'I':
                return 1;
        }
        return 0;
    }
}
