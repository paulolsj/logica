import java.util.Scanner;

public class Problema2 {
    // psvm e tab
    public static void main(String[] args) {

        Scanner leitura = new Scanner(System.in);

        System.out.println("Digite o Dia: ");
        int dia = leitura.nextInt();
        System.out.println("Digite o Mes: ");
        int mes = leitura.nextInt();
        System.out.println("Digite o Ano: ");
        int ano = leitura.nextInt();

        if(valideData(dia,mes,ano))
            System.out.println("Data Valida");
        else
            System.out.println("Data Invalida");
    }

    public static boolean valideData(int dia,int mes,int ano){
        if (ano > 0){
            if ((mes>=1) && (mes <= 12)) {
                if((dia > 0) && (dia <= diasMes(mes,ano)))
                    return true;
            }
        }
        return false;
    }

    public static int diasMes(int mes,int ano) {
        int diasM[] = {31,28,31,30,31,30,31,31,30,31,30,31};

        return (mes == 2) ? diasBissexto(ano) : diasM[mes];
    }

    public static int diasBissexto(int ano){
        if((ano % 4)==0){
            if((ano % 100)==0)
                return ((ano % 400)==0) ? 29 : 28;
            return 29;
        }
        return 28;
    }
}
