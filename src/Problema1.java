import java.util.Scanner;

public class Problema1 {
    // psvm e tab
    public static void main(String[] args) {

        Scanner leitura = new Scanner(System.in);

        System.out.println("Digite um numero: ");
        long entrada = leitura.nextLong();

        long saida = funcMister(entrada);
        System.out.printf("Valor Lido = %d Valor de Saida = %d ", entrada, saida);
    }

    public static long funcMister(long entrada){
        long pot = 1;
        long saida = 0;
        do {
            long num = entrada % 10;
            saida += pot * num;
            long rep = 0;
            if (entrada > 0) {
                long pnum;
                do {
                    entrada /= 10;
                    pnum = entrada % 10;
                    rep++;
                } while (num == pnum);
            }
            pot *= 10;
            saida += pot * rep;
            pot *= 10;
        } while (entrada > 0);
        return saida;
    }
}
